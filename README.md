## proxy

proxy可以通过公网服务器访问内网主机，目前仅支持tcp流量转发（测试通过ssh,http,mysql应用的流量转发）
注:可以通过域名或者端口进行转发

### 配置文件

#### 服务端

proxy.yaml 文件

```
port: 6666
# 服务器ip
server: 67.216.203.138
#每秒 并发量
concurrent: 1000

#如果通过域名转发，则需要指定一个共用的http转发端口
httpPort: 9090
client:
		#客户端key
        ztgreat:
        		 #访问域名（通过域名访问）
               - domain: www.ztgreat.cn
               	 #代理类型
                 proxyType: http
                 #真实主机ip
                 realhost: 127.0.0.1
                 #真实主机端口
                 realhostport: 8081
                 description: http代理
                 
                 #访问端口（通过端口访问，serverIp+端口）
               - serverport: 9091
                 proxyType: http
                 realhost: 127.0.0.1
                 realhostport: 8081
                 description: http代理

               - serverport: 3307
                 proxyType: tcp
                 realhost: 127.0.0.1
                 realhostport: 3306
                 description: mysql 代理
               - serverport: 2222
                 proxyType: tcp
                 realhost: 172.16.254.63
                 realhostport: 22
                 description: ssh 代理


```

#### 客户端

client.properties

```
# 客户端key 标识客户端
key=ztgreat

#proxy-server地址
server.host=127.0.0.1
#server.host=67.216.203.138
#server.host=47.97.111.38

#proxy-server 服务端口
server.port=6666
```

### 使用方法

#### 服务端

- 运行proxy-server 中proxyServer类即可

#### 客户端

- 运行proxy-client 中proxyClient类即可

### proxy 原理

![proxy](./pics/proxy.png)

代理客户端先和代理服务器建立连接，代理服务器通过不同的端口来区分具体的代理服务，用户通过访问代理服务器的指定端口，然后代理服务器将数据转发给代理客户端，客户端再转发数据给真实服务器，当客户端接收到真实服务器响应后，再传输给代理服务器，代理服务器再将数据传送给用户，完成一次请求。